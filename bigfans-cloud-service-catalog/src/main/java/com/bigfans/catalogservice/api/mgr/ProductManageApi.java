package com.bigfans.catalogservice.api.mgr;

import com.bigfans.catalogservice.api.request.ProductCreateRequest;
import com.bigfans.catalogservice.service.product.ProductService;
import com.bigfans.catalogservice.service.productgroup.ProductGroupService;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.plugins.FileStoragePlugin;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductManageApi extends BaseController {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductGroupService productGroupService;
    @Autowired
    private FileStoragePlugin fileStoragePlugin;

    @PostMapping(value = "/product")
    @NeedLogin(roles = {"operator" , "admin"})
    public RestResponse create(@RequestBody ProductCreateRequest request) throws Exception{
        productGroupService.create(request.getProductGroup(), request.getProducts(), request.getAttributes() , request.getTags());
        return RestResponse.ok();
    }

}
