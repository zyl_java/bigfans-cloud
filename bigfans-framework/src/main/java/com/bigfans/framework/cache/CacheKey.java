package com.bigfans.framework.cache;

import java.io.Serializable;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Mar 2, 2016 8:12:05 PM 
 *
 */
public class CacheKey implements Serializable{
	
	private static final long serialVersionUID = 2822244455980295160L;
	
	private String key;
	
	public CacheKey() {

	}
	
	public CacheKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
