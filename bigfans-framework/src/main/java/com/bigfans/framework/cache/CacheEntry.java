package com.bigfans.framework.cache;

import java.io.Serializable;

/**
 * 
 * @Description:
 * @author lichong
 * @date Mar 2, 2016 8:07:07 PM
 *
 */
public class CacheEntry implements Serializable{

	private static final long serialVersionUID = -1871695374924060333L;
	
	private CacheKey key;
	private Object value;
	private Long expire;
	private Boolean hasExpired;
	
	public CacheEntry() {}
	
	public CacheEntry(CacheKey key , Object value) {
		this.key = key;
		this.value = value;
	}
	
	public CacheEntry(CacheKey key , Object value , Long expire) {
		this.key = key;
		this.value = value;
		this.expire = expire;
	}
	
	public CacheKey getKey() {
		return key;
	}

	public void setKey(CacheKey key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Long getExpire() {
		return expire;
	}

	public void setExpire(Long expire) {
		this.expire = expire;
	}

	public Boolean getHasExpired() {
		return hasExpired;
	}

	public void setHasExpired(Boolean hasExpired) {
		this.hasExpired = hasExpired;
	}

}
