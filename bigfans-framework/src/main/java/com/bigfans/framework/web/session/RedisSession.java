package com.bigfans.framework.web.session;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import javax.servlet.http.HttpSession;

/**
 * 
 * @Description: RedisSession对象
 * @author lichong
 * @date Feb 2, 2016 8:03:31 PM 
 *
 */
public class RedisSession implements InvocationHandler{
	
	private HttpSession httpSession;
	
	public RedisSession(HttpSession httpSession) {
		this.httpSession = httpSession;
	}
	
//	private RedisService redisService = BeanProviderFactory.getProvider().getBean(RedisService.class);

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//		if("getAttribute".equals(method.getName())){
//			String key = (String) args[0];
//			if("sessionUser".equals(key)){
//				return redisService.getSessionUser(CookieHolder.get("user_key"));
//			}
//		}
//		if("invalidate".equals(method.getName())){
//			redisService.removeAllAttributes(CookieHolder.get("user_key"));
//			return null;
//		}
		return method.invoke(httpSession, args);
	}
}
