package com.bigfans.orderservice.model;

import com.bigfans.framework.utils.DateUtils;
import com.bigfans.orderservice.model.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 
 * @Description:订单条目
 * @author lichong 
 * 2014年12月21日下午6:42:07
 *
 */
@Data
public class OrderItem extends OrderItemEntity {

	private static final long serialVersionUID = -5373646933676090627L;
	
	/* 产品名称 */
	private String prodName;
	/* 产品图片 */
	private String prodImg;
	private String pgId;
	private List<OrderItemSpec> specs;
	private List<String> promotions;
	private BigDecimal originalPrice;
	private BigDecimal originalSubTotal;

	public String getCreateDateStr() {
		return DateUtils.toStringWithFormat(createDate, "yyyy-MM-dd HH:mm:ss");
	}
	
	public Boolean getIsCommented(){
		return commentStatus == 1;
	}

}
