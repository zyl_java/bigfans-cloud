package com.bigfans;

import java.math.BigDecimal;

/**
 * 
 * @Description:实体类的常量定义
 * @author lichong
 * 2014年12月14日下午4:52:07
 *
 */
public interface Constants {
	
	String SPACE = " ";
	BigDecimal ZERO = new BigDecimal(0);
	BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	String REQ_PAGESIZE = "ps";
	String REQ_CUR_PAGE = "cp";

	interface OAUTH {
		String HEADER_KEY_NAME = "Authorization";
	}

	interface TOKEN {
		String HEADER_KEY_NAME = "Token";
		String JWT_SECURITY_KEY = "bigfans";
		String TMP_COOKIE_NAME = "tmp_token";
	}

	interface SERVICE{
		String PRODUCT_SERVICE_NAME = "catalog-service";
		String PRODUCT_SERVICE_URL = "http://catalog-service";
	}
	
}
