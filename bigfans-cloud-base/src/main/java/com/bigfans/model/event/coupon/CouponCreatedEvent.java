package com.bigfans.model.event.coupon;

import com.bigfans.framework.event.AbstractEvent;
import lombok.Data;

@Data
public class CouponCreatedEvent extends AbstractEvent {

    private String couponId;

    public CouponCreatedEvent(String couponId) {
        this.couponId = couponId;
    }
}
