package com.bigfans.reviewservice;

import com.bigfans.framework.Applications;
import com.bigfans.framework.CurrentUser;

/**
 * @author lichong
 * @create 2018-04-27 下午9:00
 **/
public class ReviewApplications extends Applications {

    private static String functionalUserToken;
    private static CurrentUser functionalUser;

    public static void setFunctionalUser(CurrentUser functionalUser) {
        ReviewApplications.functionalUser = functionalUser;
    }

    public static CurrentUser getFunctionalUser() {
        return functionalUser;
    }

    public static void setFunctionalUserToken(String functionalUserToken) {
        ReviewApplications.functionalUserToken = functionalUserToken;
    }

    public static String getFunctionalUserToken() {
        return functionalUserToken;
    }
}
